﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scripCube : MonoBehaviour
{
    public Script_Lampe script;
    private void OnCollisionEnter(Collision other)
    {


        if (other.gameObject.CompareTag("cube2"))
        {
            script.AluumerLampe(true);

        }


    }
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("cube2"))
        {
            script.AluumerLampe(false);
        }

    }
}

