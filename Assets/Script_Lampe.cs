﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Lampe : MonoBehaviour
{
    public Material Rouge;
    public Material Vert;
    public int nb = 0;
    public bool dooropenning;
    public GameObject doorleft;
    public GameObject Door_Right_dep;
    public GameObject artefact;
    private float dep;
    public GameObject taches;
    public GameObject taches2;
    public GameObject roomWin;
    public AudioClip sound;
    public AudioClip sound2;
    private AudioSource source { get { return GetComponent<AudioSource>(); } }
    private AudioSource source2 { get { return GetComponent<AudioSource>(); } }


    public void AluumerLampe(bool b)

    {
        if (b)
        {
            nb++;
            
        }

        else
        {
            nb--;
        }
        if (nb==2)
        {
            taches2.SetActive(false);
            taches.SetActive(true);
        }


        if (nb == 3)
        {
            GameObject lampe = GameObject.Find("lampe1");
            Material[] mats = lampe.GetComponent<MeshRenderer>().materials;
            mats[1] = Vert;
            lampe.GetComponent<MeshRenderer>().materials = mats;
            PlaySound2();
            artefact.SetActive(true);
            //taches.SetActive(false);

        }
    }
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;
        gameObject.AddComponent<AudioSource>();
        source2.clip = sound2;
        source2.playOnAwake = false;
        dep = Door_Right_dep.transform.position.x;


    }

    void Update()
    {

        if (dooropenning)
        {
            if (Door_Right_dep.transform.position.x > dep + Door_Right_dep.GetComponentInChildren<MeshRenderer>().bounds.size.x)
            {

                dooropenning = false;
                
            }
            else
            {
                Door_Right_dep.transform.Translate(Vector3.right * Time.deltaTime * 5);
                doorleft.transform.Translate(Vector3.right * Time.deltaTime * 5);
            }
        }



    }

    public void ouvrirporte()
    {
        if (nb == 3)
        {
            GameObject lampe = GameObject.Find("lampe2");
            Material[] mats = lampe.GetComponent<MeshRenderer>().materials;
            mats[1] = Vert;
            lampe.GetComponent<MeshRenderer>().materials = mats;

            GameObject col = GameObject.Find("collider");
            col.SetActive(false);
            dooropenning = true;
            PlaySound();
            roomWin.SetActive(true);
            taches.SetActive(false);



        }
    }


    void PlaySound()
    {
        source.PlayOneShot(sound);
    }
    void PlaySound2()
    {
        source.PlayOneShot(sound2);
    }
}
