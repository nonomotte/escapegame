﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DoorController : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject doorleft;
    public GameObject Door_Right_dep;
    public bool dooropenning;
    private float dep;
    private int compteur = 0;
    public int nbObject = 1;
    public AudioClip sound;
    private AudioSource source { get { return GetComponent<AudioSource>(); } }
    public GameObject taches;


    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;
        dep = Door_Right_dep.transform.position.x;
        

    }

    // Update is called once per frame
    void Update()
    {
       
        if (dooropenning)
        {
            if (Door_Right_dep.transform.position.x > dep + Door_Right_dep.GetComponentInChildren<MeshRenderer>().bounds.size.x)
            {
                
                dooropenning = false;
                taches.SetActive(true);
            }
            else
            {
                Door_Right_dep.transform.Translate(Vector3.right * Time.deltaTime * 5);
                doorleft.transform.Translate(Vector3.right * Time.deltaTime * 5);
            }
        }



    }

    void PlaySound()
    {
        source.PlayOneShot(sound);
    }

    public void add()
    {
        compteur++;
        if (compteur == nbObject)
        {
            dooropenning = true;
            PlaySound();

        }
        else
        {
            dooropenning = false;
        }
    }

  

    public void remove()
    {
        compteur--;
        if (compteur == nbObject)
        {
            dooropenning = true;

        }
        else
        {
            dooropenning = false;
        }
    }



}
