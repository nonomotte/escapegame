﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_sphere : MonoBehaviour
{
    

    public Script_Lampe script;
    private void OnCollisionEnter(Collision collision)
    {
        

        if (collision.gameObject.CompareTag("sphere2"))
        {
            script.AluumerLampe(true);
            
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("sphere2"))
        {
            script.AluumerLampe(false);
        }
    }
   

}
